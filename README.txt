
Media Player

This module provides a home-grown flash media player for video and audio
playback. It's written in OpenLaszlo, making modification easy, making
modifications relatively easy for developers. It plans to provide an
extensive API for custom administrative modifications as well.
