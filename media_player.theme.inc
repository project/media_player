<?php

/**
 *  @file
 *  Theme functions for the Media Player module.
 */

/**
 *  Display a file embedded in a media player.
 *  @param $file
 *    The path to the media to play.
 *  @param $options
 *    An array of options to pass to the player:
 *    'absolute' => TRUE or FALSE. If TRUE, this is used when building URL's (useful for embedding externally).
 *    'autoplay' => TRUE or FALSE
 *    'width' => The width of the player
 *    'height' => The height of the player
 *    'color_bg' => The background color of the player, such as 'red', 'black', or a Hex value.
 *    'display_logo' => TRUE or FALSE
 *    'logo' => The path to an icon to display
 *    'logo_x' => The x coordinate to display the logo
 *    'logo_y' => The y coordinate to display the logo
 *    'display_image' => TRUE or FALSE
 *    'image' => The path to an image to display before playing the media
 *    'image_x' => The x coordinate to display the image
 *    'image_y' => The y coordinate to display the image
 *  @param $params
 *    An array of parameters to pass to the flash
 *    'quality' => The quality of the flash, such as 'high'
 *    'scale' => Whether to allow the flash to scale, such as 'noscale' or 'scale'
 *  @param $player
 *    If NULL, then this will route to the default player.
 *    Otherwise, it will route to the specified player, from one of the following:
 *      'media_player' => The included flash media player, created specifically for Drupal using OpenLaszlo.
 *      'jw_player' => JW Media Player.
 *      'wimpy' => Wimpy Media Player.
 */
function theme_media_player_player($file, $options = array(), $params = array(), $player = NULL) {
  $player = isset($player) ? $player : variable_get('media_player_default_player', 'media_player');
  switch ($player) {
    case 'media_player':
    default:
      return theme('media_player_media_player', $file, $options);
  }
}

function theme_media_player_media_player($file, $options = array(), $params = array(), $id = NULL) {
  static $count = 0;
  _media_player_default_options($options, $params);
  $logo = $options['display_logo'] ? "&display_logo={$options['display_logo']}&logo={$options['logo']}" : '';
  $image = $options['image'] ? "&image={$options['image']}" : '';
  $color_bg = "&color_bg={$options['color_bg']}";
  $url_options = array(
    'absolute' => $options['absolute'],
    'query' => "lzproxied=false&file=$file$logo$color_bg$image",
  );
  $url = variable_get('media_player_path_media_player', drupal_get_path('module', 'media_player') .'/media_player.lzx.lzr=swf8.swf');
  $url = url($url, $url_options);
  $params['movie'] = $url;
  $id = isset($id) ? $id : 'media-player-'. ($count++);
  foreach ($params as $key => $param) {
    $parameters .= "<param name='$key' value='$param'>";
  }
  drupal_add_js(drupal_get_path('module', 'media_player') .'/lps/includes/embed-compressed.js');
  $output .= <<<LWZ
  <script type="text/javascript">
    lz.embed.swf({url: '$url', bgcolor: '{$options['color_bg']}', width: '{$options['width']}', height: '{$options['height']}', id: '$id', accessible: 'false'});
  </script><noscript>
    Please enable JavaScript in order to use this application.
  </noscript>
LWZ;
//   $output .= "<object type='application/x-shockwave-flash' data='$url' width='{$options['width']}' height='{$options['height']}'>$parameters</object>";
  return $output;
}

function _media_player_default_options(&$options, &$params) {
  // $options
  $options['absolute'] = isset($options['absolute']) ? $options['absolute'] : variable_get('media_player_default_absolute', FALSE);
  $options['autoplay'] = isset($options['autoplay']) ? $options['autoplay'] : variable_get('media_player_default_autoplay', FALSE);
  $options['width'] = isset($options['width']) ? $options['width'] : variable_get('media_player_default_width', 500);
  $options['height'] = isset($options['height']) ? $options['height'] : variable_get('media_player_default_height', 400);
  $options['color_bg'] = isset($options['color_bg']) ? $options['color_bg'] : variable_get('media_player_default_color_bg', 'black');
  $options['display_logo'] = isset($options['display_logo']) ? $options['display_logo'] : variable_get('media_player_default_display_logo', TRUE);
  $options['logo'] = isset($options['logo']) ? $options['logo'] : variable_get('media_player_default_logo', drupal_get_path('module', 'media_player') .'/resources/druplicon.png');
  $options['logo'] = url($options['logo'], array('absolute' => $options['absolute']));
  $options['logo_x'] = isset($options['logo_x']) ? $options['logo_x'] : variable_get('media_player_default_logo_x', 400);
  $options['logo_y'] = isset($options['logo_y']) ? $options['logo_y'] : variable_get('media_player_default_logo_y', 10);
  $options['display_image'] = isset($options['display_image']) ? $options['display_image'] : variable_get('media_player_default_display_image', TRUE);
  $options['image'] = isset($options['image']) ? $options['image'] : variable_get('media_player_default_image', drupal_get_path('module', 'media_player') .'/resources/druplicon.png');
  $options['image'] = url($options['image'], array('absolute' => $options['absolute']));
  $options['image_x'] = isset($options['image_x']) ? $options['image_x'] : variable_get('media_player_default_image_x', 0);
  $options['image_y'] = isset($options['image_y']) ? $options['image_y'] : variable_get('media_player_default_image_y', 0);

  // $params
  $params['quality'] = isset($params['quality']) ? $params['quality'] : variable_get('media_player_default_quality', 'high');
  $params['scale'] = isset($params['scale']) ? $params['scale'] : variable_get('media_player_default_scale', 'noscale');
  $params['salign'] = isset($params['salign']) ? $params['salign'] : variable_get('media_player_default_salign', 'LT');
  $params['menu'] = isset($params['menu']) ? $params['menu'] : variable_get('media_player_default_menu', 'false');
}
